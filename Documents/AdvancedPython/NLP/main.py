from typing import Optional

import nltk
import pandas as pd
import string
import re

from pandas import DataFrame
from matplotlib import pyplot
import numpy as np
from tabulate import tabulate
import nltk
# nltk.download()
# nltk.download('wordnet')
from nltk.corpus import stopwords
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import KFold, cross_val_score
from sklearn.metrics import precision_recall_fscore_support as score
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV


wn = nltk.WordNetLemmatizer()
ps = nltk.PorterStemmer()

rawdata = open("Ex_Files_NLP_Python_ML_EssT/Exercise Files/Ch01/01_03/Start/SMSSpamCollection.tsv").read()
parsedData = rawdata.replace('\t', '\n').split('\n')
labelList = parsedData[0::2]
textList = parsedData[1::2]

pd.set_option('display.max_colwidth', 100)
fullCorpus: DataFrame = pd.DataFrame({'label': labelList[:-1], 'body_list': textList})


# print("Out of {} rows, {} are spam, {} are ham".format(len(fullCorpus),
#                                                         len(fullCorpus[fullCorpus['label']=='spam']),
#                                                         len(fullCorpus[fullCorpus['label']=='ham'])))
#
# print("Number of null in label: {}".format(fullCorpus['label'].isnull().sum()))
# print("Number of null in text: {}".format(fullCorpus['body_list'].isnull().sum()))

# PREPROCESSING TEXT DATA
## 1) Remove punctuation
## 2) Tokenization
## 3) Remove stopwords
## 4) Lemmatize/Stem

# 1
def remove_punct(text):
    text_punc = ''.join([char for char in text if char not in string.punctuation])
    return text_punc


fullCorpus['body_text_clean'] = fullCorpus['body_list'].apply(lambda x: remove_punct(x))


# 2

def tokenize(text):
    tokens = re.split('\W+', text)
    return tokens


fullCorpus['body_text_tokenized'] = fullCorpus['body_text_clean'].apply(lambda x: tokenize(x.lower()))

# 3

stopword = stopwords.words('english')


def remove_stopwords(tokenized_list):
    text = [word for word in tokenized_list if word not in stopword]
    return text


fullCorpus['body_text_nostop'] = fullCorpus['body_text_tokenized'].apply(lambda x: remove_stopwords(x))


# STEM
def stemming(tokenized_text):
    text = [ps.stem(word) for word in tokenized_text]
    return text


# LEMM
def lemmatizing(tokenized_text):
    text = [wn.lemmatize(word) for word in tokenized_text]
    return text


fullCorpus['body_text_stemm'] = fullCorpus['body_text_nostop'].apply(lambda x: stemming(x))


# CleanText
def clean_text(text):
    text = "".join([word.lower() for word in text if word not in string.punctuation])
    tokens = re.split('\W+', text)
    text = [ps.stem(word) for word in tokens if
            word not in stopword]  # for N-grams - " ".join([ps.stem(word) for word in tokens if word not in stopword])
    return text

def count_punct(text):
    count = sum([1 for char in text if char in string.punctuation])
    return round(count / (len(text) - text.count(" ")), 3) * 100


fullCorpus['punct%'] = fullCorpus['body_list'].apply(lambda x: count_punct(x))
# Create feature for text message length
fullCorpus['body_len'] = fullCorpus['body_list'].apply(lambda x: len(x) - x.count(" "))

# Apply N-Grams
# fullCorpus['cleaned_text'] = fullCorpus['body_list'].apply(lambda x: clean_text(x))
#
# ngram_vect = CountVectorizer(ngram_range=(2, 2))
# X_counts = ngram_vect.fit_transform(fullCorpus[0:20]['cleaned_text'])

# X_counts_df = pd.DataFrame(X_counts.toarray())
# X_counts_df.columns = ngram_vect.get_feature_names_out()


# tf-idf Equation
tfidf_vect = TfidfVectorizer(analyzer=clean_text)
X_tfidf = tfidf_vect.fit_transform(fullCorpus['body_list'])
X_tfidf = pd.DataFrame(X_tfidf.toarray())
X_tfidf.columns = tfidf_vect.get_feature_names_out()
X_tfidf_feat = pd.concat([fullCorpus['body_len'], fullCorpus['punct%'], X_tfidf], axis=1)

# Apply CountVectorizer
count_vect = CountVectorizer(analyzer=clean_text)
X_count = count_vect.fit_transform(fullCorpus['body_list'])
X_count = pd.DataFrame(X_count.toarray())
X_count.columns = count_vect.get_feature_names_out()
X_count_feat: Optional[DataFrame] = pd.concat([fullCorpus['body_len'], fullCorpus['punct%'], X_count], axis=1)

# MATPLOTLIB
# bins = np.linspace(0,50,40)
# pyplot.hist(fullCorpus['spam' == fullCorpus['label']]['punct%'], bins, alpha=0.5, label='spam')
# pyplot.hist(fullCorpus['ham' == fullCorpus['label']]['punct%'], bins, alpha=0.5, label='ham')
# pyplot.legend(loc='upper left')
# pyplot.show()
# DETERMINE Transformation
# for i in [1,2,3,4,5]:
#     pyplot.hist((fullCorpus['punct%'])**(1/i),bins=40)
#     pyplot.title("Transformation: 1/{}".format(str(i)))
#     pyplot.show()

rf = RandomForestClassifier()
param = {'n_estimators': [10, 150, 300],
         'max_depth': [30, 60, 90, None]}
gs = GridSearchCV(rf, param, cv=5, n_jobs=-1)
gs_fit = gs.fit(X_tfidf_feat,fullCorpus['label'])
gs_results_tfidf = pd.DataFrame(gs_fit.cv_results_).sort_values('mean_test_score',ascending=False)[0:5]


rf = RandomForestClassifier()
param = {'n_estimators': [10, 150, 300],
         'max_depth': [30, 60, 90, None]}
gs = GridSearchCV(rf, param, cv=5, n_jobs=-1)
gs_fit = gs.fit(X_count_feat,fullCorpus['label'])
gs_results_count = pd.DataFrame(gs_fit.cv_results_).sort_values('mean_test_score',ascending=False)[0:5]


# X_train, X_test, y_train, y_test = train_test_split(X_features, fullCorpus['label'], test_size=0.2)
#
# def train_RF(n_est, depth):
#     rf: RandomForestClassifier = RandomForestClassifier(n_estimators=n_est, max_depth=depth, n_jobs=-1)
#     rf_model: object = rf.fit(X_train, y_train)
#     y_pred = rf_model.predict(X_test)
#     precision, recall, fscore, support = score(y_test, y_pred, pos_label='spam', average='binary')
#     print("Est: {} / Depth: {} ---- Precision: {} / Recall: {} / Accuracy: {}".format(n_est, depth, round(precision, 3),
#                                                                                       round(recall, 3), round(
#             (y_pred == y_test).sum() / len(y_pred), 3)))
#
#
# for n_est in [10, 50, 100]:
#     for depth in [10, 20, 30, None]:
#         train_RF(n_est, depth)

# print(sorted(zip(rf_model.feature_importances_,X_train.columns),reverse=True)[0:10])
# print('Precision: {} / Recall: {} / Accuracy: {}'.format(precision,round(recall,3),round((y_pred==y_test).sum()/len(y_pred),3)))
# k_fold = KFold(n_splits=5)
# print(cross_val_score(rf, X_features, fullCorpus['label'], cv=k_fold, scoring='accuracy', n_jobs=-1))
print(tabulate(gs_results_tfidf.head(1), headers='keys'))
print(tabulate(gs_results_count.head(1), headers='keys'))


